<?php

function BXIBlockAfterSave($arFields)
{
	// $db_props = CIBlockElement::GetProperty($arFields['IBLOCK_ID'], $arFields['ID'], array('sort' => 'asc'), array('CODE' => 'YOUTUBE_CODE' ));
	$db_props = CIBlockElement::GetList(
		array(), 
		array('IBLOCK_ID' => $arFields['IBLOCK_ID'], 'ID' => $arFields['ID']), 
		false, 
		false, 
		array('PROPERTY_YOUTUBE_CODE', 'PROPERTY_YOUTUBE_PREVIEW')
	);

	if ($ar_props = $db_props->Fetch()) {
		// создавать превью, только если его нет
		if (!empty($ar_props['PROPERTY_YOUTUBE_CODE_VALUE']) && empty($ar_props['PROPERTY_YOUTUBE_PREVIEW_VALUE'])) {

			$preview_sizes = array('maxres', 'sd', 'hq', 'mq', '');
			$youtube_code = $ar_props['PROPERTY_YOUTUBE_CODE_VALUE'];
			$best_url = false;
	
			foreach ($preview_sizes as $size) {
	
				$url = "http://img.youtube.com/vi/{$youtube_code}/{$size}default.jpg";
				$ch = curl_init($url);
				curl_setopt($ch, CURLOPT_HEADER, true);
				curl_setopt($ch, CURLOPT_NOBODY, true);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
				curl_setopt($ch, CURLOPT_TIMEOUT,10);
				$output = curl_exec($ch);
				$http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
				curl_close($ch);
				
				if (200 == $http_code) {
					$best_url = $url;
					break;
				}
			}

			if (false !== $best_url) {

				$ar_file = CFile::MakeFileArray($best_url);
				CIBlockElement::SetPropertyValues($arFields["ID"], $arFields["IBLOCK_ID"], $ar_file, "YOUTUBE_PREVIEW");
			}
		}
	}
}

