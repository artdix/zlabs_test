<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

const IMAGE_MAX_WIDTH  = 960;

$detail_text = $arResult['DETAIL_TEXT'];

if (isset($arResult['PROPERTIES']['MORE_PHOTO'])) {

    if (0 < count($arResult['PROPERTIES']['MORE_PHOTO']['VALUE'])) {

        foreach ($arResult['PROPERTIES']['MORE_PHOTO']['VALUE'] as $key => $ID) {

            $i = $key + 1;
            $image_info = CFile::GetFileArray($ID);
            $image_alt = GetMessage('ZLABS_TEST_MORE_PHOTO_ALT').'-'.$i;
            $image = CFile::ResizeImageGet($ID, array('width' => IMAGE_MAX_WIDTH, 'height' => $image_info['HEIGHT']), BX_RESIZE_IMAGE_PROPORTIONAL, true);

            if ($image) {

                $img = '<img src="'.$image['src'].'" width="'.$image['width'].'" height="'.$image['height'].'" class="inserted" alt="'.$image_alt.'">';
                $detail_text = str_replace("#IMAGE_$i#", $img, $detail_text);
            }
        }
    }
}

if (
    isset($arResult['PROPERTIES']['YOUTUBE_CODE']) && !empty($arResult['PROPERTIES']['YOUTUBE_CODE']['VALUE']) &&
    isset($arResult['PROPERTIES']['YOUTUBE_PREVIEW']) && !empty($arResult['PROPERTIES']['YOUTUBE_PREVIEW']['VALUE'])
) {

    $yputube_code = $arResult['PROPERTIES']['YOUTUBE_CODE']['VALUE'];
    $image_info = CFile::GetFileArray($arResult['PROPERTIES']['YOUTUBE_PREVIEW']['VALUE']);
    $image = CFile::ResizeImageGet(
        $arResult['PROPERTIES']['YOUTUBE_PREVIEW']['VALUE'], 
        array('width' => IMAGE_MAX_WIDTH, 'height' => $image_info['HEIGHT']), 
        BX_RESIZE_IMAGE_PROPORTIONAL, 
        true
    );

    if ($image) {
        $image_alt = GetMessage('ZLABS_TEST_YOUTUBE_PREVIEW_ALT');
        $onclick = "onclick=\"iframe = document.createElement('iframe');iframe.src ='//www.youtube.com/embed/$yputube_code?rel=0&autoplay=1';iframe.onclick = function () {this.parentElement.removeChild(this);};
        this.parentNode.insertBefore(iframe, this.nextSibling);\"";
        $img = '<div class="youtube_preview_wrap"><img id="youtube_preview" src="'.$image['src'].'" alt="'.$image_alt.'" width="'.$image['width'].'" height="'.$image['height'].'" '.$onclick.'></div>';
        $detail_text = str_replace("#YOUTUBE#", $img, $detail_text);
    }
}

$arResult['DETAIL_TEXT'] = $detail_text;

if (\Bitrix\Main\Loader::includeModule('dev2fun.opengraph')) {

    \Dev2fun\Module\OpenGraph::Show($arResult['ID'], 'element');

    if (isset($arResult['PREVIEW_PICTURE']['SRC']) && !empty($arResult['PREVIEW_PICTURE']['SRC'])) {

        $APPLICATION->SetPageProperty('og:image', SITE_SERVER_NAME.$arResult['PREVIEW_PICTURE']['SRC']);
    }
}

